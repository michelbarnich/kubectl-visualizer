const {execSync} = require('child_process');

class cmd {
    /**
     * execute shell command
     * @param {*} cmd command as string
     * @returns command output as a string
     */
    execute(cmd) {
        return execSync(cmd).toString("utf-8");
    }
}

module.exports = cmd