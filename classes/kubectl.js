const commandExistsSync = require('command-exists').sync;

const Cmd = require("./cmd.js")
const cmd = new Cmd();


class kubectl {

    constructor() {
        // verify that kubectl command exists
        if(!this.exists()) throw new Error('kubectl does not appear to be installed on this system.');
    }

    /**
     * checks if the command kubectl is available on the system
     * @returns boolean value
     */
    exists() {
        return commandExistsSync("kubectl");
    }
    
    /**
     * list namespaces in context defined by kubeconfig
     * @returns array containing namespaces
     */
     namespaces() {
        var names = [];
        var cmd_response = JSON.parse(cmd.execute("kubectl get namespace -o json"));
        
        for(var i = 0; i < cmd_response.items.length; i++) {
            let namespace = cmd_response.items[i];
            names.push(namespace.metadata.name)
        }

        return names;
    }

    /**
     * get Pod names for namespace
     * @param {*} namespace name of namespace as a string, if undefined, all pods will be shown
     * @returns array of pod names
     */
    pods(namespace=undefined) {
        var names = [];
        var cmd_response = (namespace != undefined) ? JSON.parse(cmd.execute("kubectl get pods --namespace " + namespace + " -o json")) : JSON.parse(cmd.execute("kubectl get pods -A -o json"));
        
        for(var i = 0; i < cmd_response.items.length; i++) {
            let pod = cmd_response.items[i];
            names.push(pod.metadata.name)
        }

        return names;
    }

    /**
     * get a json representation of cluster
     * @returns JSON object representing cluster
     */
    cluster() {
        var cluster = {}
        var namespaces = this.namespaces()

        for(var i = 0; i < namespaces.length; i++) {
            let pods_for_namespace = this.pods(namespaces[i])
            cluster[namespaces[i]] = {}
            cluster[namespaces[i]] = pods_for_namespace;
        }

        return cluster
    }
}

module.exports = kubectl;