const commandExistsSync = require('command-exists').sync;
const builder = require('xmlbuilder');

const Cmd = require("./cmd.js")
const cmd = new Cmd();

class drawio {

    /*constructor() {
        // verify that kubectl command exists
        if(!this.exists()) throw new Error('drawio does not appear to be installed on this system.');
    }

    /**
     * checks if the command kubectl is available on the system
     * @returns boolean value
     */
    /*exists() {
        return commandExistsSync("drawio");
    }*/

    /**
     * renders XML from cluster object
     * @param {*} cluster json object representing the cluster 
     * @returns XML formatted string
     */
    render_XML(cluster) {

        let cluster_namespcaes = Object.keys(cluster);
        var xml = builder.create("mxfile")
            .att("host", "app.diagrams.net")
            .att("modified", "2022-08-10T11:06:44.308Z")
            .att("agent", "5.0 (X11)")
            .att("etag", "xy40K5ayzUoVAjJw4eMr")
            .att("version", "17.4.6")

        var diagram = xml.ele("diagram", {
            id:"NOyR-hvUttpmt6anfqek",
            name:"Page-1"
        })
        
        var graphModel = diagram.ele("mxGraphModel", {
            dx: 1422,
            dy: 820,
            grid:1,
            gridSize:10,
            guides:1,
            connect:1,
            arrows:1,
            fold:1,
            page:1,
            pageScale:1,
            pageWidth:850,
            pageHeight:1100,
            math:0,
            shadow:0
        }).ele("root")

        graphModel.ele("mxCell", {id:0})
        graphModel.ele("mxCell", {id:1, parent:0})

        for(var i = 0; i < cluster_namespcaes.length; i++) {
            // create group
            graphModel.ele("mxCell", {
                id:"group-" + i,
                value:"",
                style:"group",
                vertex:1,
                connectable:0,
                parent:1
            }).ele("mxGeometry", {
                x:(170*i) + 10,
                y:0,
                width:160,
                height:(cluster[cluster_namespcaes[i]].length * 80) + 80,
                as:"geometry"
            })

            //create box
            graphModel.ele("mxCell", {
                id:"box-" + i,
                value:"",
                style:"rounded=0;whiteSpace=wrap;html=1",
                vertex:1,
                parent:"group-" + i
            }).ele("mxGeometry", {
                width:160,
                height:(cluster[cluster_namespcaes[i]].length * 80) + 80,
                as:"geometry"
            })

            //create header
            graphModel.ele("mxCell", {
                id:"text-" + i,
                value:"<font style=\"font-size: 19px\"><b>" + cluster_namespcaes[i] + "</b></font>",
                style:"text;html=1;strokeColor=none;fillColor=none;align=center;verticalAlign=middle;whiteSpace=wrap;rounded=0;",
                vertex:1,
                parent:"group-" + i
            }).ele("mxGeometry", {
                x:0,
                y:0,
                width:160,
                height:80,
                as:"geometry"
            })

            for(var j = 0; j < cluster[cluster_namespcaes[i]].length; j++) {
                //create box inside box
                graphModel.ele("mxCell", {
                    id:"box-" + i + "-box-" + j,
                    value:"",
                    style:"rounded=0;whiteSpace=wrap;html=1",
                    vertex:1,
                    parent:"group-" + i
                }).ele("mxGeometry", {
                    width:160,
                    y:80 + (80*j),
                    height:80,
                    as:"geometry"
                })

                //create header
                graphModel.ele("mxCell", {
                    id:"box-" + i + "-text-" + j,
                    value:cluster[cluster_namespcaes[i]][j],
                    style:"text;html=1;strokeColor=none;fillColor=none;align=center;verticalAlign=middle;whiteSpace=wrap;rounded=0;",
                    vertex:1,
                    parent:"group-" + i
                }).ele("mxGeometry", {
                    x:0,
                    y:80 + (80*j),
                    width:160,
                    height:80,
                    as:"geometry"
                })
            }

        }

        return xml.end({pretty:true});
    }

}

module.exports = drawio;