const Kubectl = require("./classes/kubectl.js");
const Drawio = require("./classes/drawio.js");
const fs = require("fs")

const kubectl = new Kubectl();
const drawio = new Drawio();

function main() {
    let cluster = kubectl.cluster()
    let xml = drawio.render_XML(cluster);

    fs.writeFileSync("cluster.xml", xml);

}

main()